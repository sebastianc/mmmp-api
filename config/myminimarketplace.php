<?php

return [


    /*
     * The version of the app, used for force updating.
     * Leave off any trailing zeros. EG 2.0.0 becomes 2, 2.1.0 becomes 2.1
     */

    'version' => [
        'android' => '1',
        'ios' => '1'
    ],

    /*
     * My Mini Market place fee.
     *
     * If item sells for 5 pounds and fee is set to 0.1 MMMP will get 0.50
     */

    'fee' => 0.10

];