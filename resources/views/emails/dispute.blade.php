<h1>My Mini Marketplace</h1>
<h2>A dispute has been raised for product #{{ $dispute->purchase->product->id }} on {{ $dispute->created_at->format('d/m/Y') }}</h2>
<h3>Product information</h3>
<ul>
    <li><b>Name:</b> {{ $dispute->purchase->product->name }}</li>
    <li><b>Description:</b> {{ $dispute->purchase->product->sizes->description }}</li>
    <li><b>Price</b> £{{ number_format($dispute->purchase->price, 2) }}</li>
    <li><b>Transaction ID:</b> {{ $dispute->purchase->stripeTransaction->charge_id }}</li>
</ul>
<h3>Seller information</h3>
<ul>
    <li><b>Username:</b> {{ $dispute->purchase->product->sellerUser->username }}</li>
    <li><b>Email:</b> {{ $dispute->purchase->product->sellerUser->email }}</li>
    <li><b>Name:</b> {{ $dispute->purchase->product->sellerUser->name }}</li>
    <li><b>Rating:</b> {{ $dispute->purchase->product->sellerUser->average_rating }}/5</li>
</ul>
<h3>Buyer information</h3>
<ul>
    <li><b>Username:</b> {{ $dispute->purchase->user->username }}</li>
    <li><b>Email:</b> {{ $dispute->purchase->user->email }}</li>
    <li><b>Name:</b> {{ $dispute->purchase->user->name }}</li>
    <li><b>Rating:</b> {{ $dispute->purchase->user->average_rating }}/5</li>
</ul>
<h3>Dispute</h3>
<p>{{ $dispute->message }}</p>