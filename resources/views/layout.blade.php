<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Mini Marketplace</title>

    <!-- Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> -->

    <!-- Styles -->
    @yield('header')
</head>
<body>
<div class="flex-center position-ref full-height">
    @yield('content')
</div>
<div class="flex-center position-ref full-height">
    @yield('footer')
</div>
</body>
</html>
