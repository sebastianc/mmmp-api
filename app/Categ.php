<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 15:38
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Categ extends Model
{

    /**
     * The stripe account associated with the users account
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function subcategories()
    {
        return $this->hasMany('App\Subcateg');
    }

}