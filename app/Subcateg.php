<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 15:38
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Subcateg extends Model
{

    public function category(){
        return $this->belongsTo('App\Categ', 'categ_id');
    }

}