<?php

namespace App;

use Carbon\Carbon;
use App\Categ;
use App\Subcateg;
use App\Type;
use App\Country;
use App\Gender;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class Product extends Model
{
    use SoftDeletes;

    protected $with = ['sizes', 'countries', 'genders', 'type_info', 'categ_info', 'subcateg_info', 'images', 'seller', 'reviewByBuyer', 'reviewBySeller'];
    protected $appends = ['comment_count', 'like_count'];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();

        if(isset($array['review_by_buyer'])){
            if(count($array['review_by_buyer'])){
                $array['review_by_buyer'] = true;
            }else{
                $array['review_by_buyer'] = false;
            }
        }else{
            $array['review_by_buyer'] = false;
        }
        if(isset($array['review_by_seller'])){
            if(count($array['review_by_seller'])){
                $array['review_by_seller'] = true;
            }else{
                $array['review_by_seller'] = false;
            }
        }else{
            $array['review_by_seller'] = false;
        }
        /*if(isset($array['accepted_offer'])){
            if($array['accepted_offer']){
                $array['cost'] = $array['accepted_offer']['amount'];
                $array['accepted_offer'] = true;
            }else{
                $array['accepted_offer'] = false;
            }
        }else{
            $array['accepted_offer'] = false;
        }

        if(isset($array['offers'])){
            $array['offers'] = count($array['offers']);
        }*/
        if(isset($array['liked_by'])){
            if($array['liked_by']){
                $array['liked_by'] = true;
            }else{
                $array['liked_by'] = false;
            }
        }

        if(isset($array['images'])){
            foreach($array['images'] as $index => $image){
                $url = $image['base_url'].'/'.$image['public_url'];
                $dimensions = [
                    'thumb' => str_replace('/original/', '/medium/', $url),
                    'large' => str_replace('/original/', '/ios/', $url),
                    'full' => $url,
                ];
                $array['images'][$index]['dimensions'] = $dimensions;
            }
        }
        return $array;
    }


    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 50,
        'slug' => 50,
        //'categ_info.name' => 32,
        //'subcateg_info.name' => 52,
        'description' => 350,
    ];

    protected $fillable = ['view_count'];


    function likes(){
        return $this->hasMany('App\ProductLike')->with('user');
    }

    function likedBy(){
        $userId = Auth::check() ? Auth::user()->id : null;
        return $this->hasMany('App\ProductLike')->where('user_id', $userId);
    }

    function comments(){
        return $this->hasMany('App\Comment')->with('user')->orderBy('created_at', 'desc')->take(2);
    }

    function webComments(){
        return $this->hasMany('App\Comment')->with('user')->orderBy('created_at', 'desc')->take(10);
    }

    function commentCount(){
        return $this->hasMany('App\Comment');
    }

    function categ_info(){
        return $this->belongsTo('App\Categ', 'categ');
    }

    function subcateg_info(){
        return $this->belongsTo('App\Subcateg', 'subcateg');
    }

    function type_info(){
        return $this->belongsTo('App\Type', 'type');
    }

    function images()
    {
        return $this->hasMany('App\ProductImage', 'product_id')->orderBy('created_at', 'asc');
    }

    function seller()
    {
        return $this->belongsTo('App\UserSearchable', 'seller', 'id');
    }

    function sellerUser()
    {
        return $this->seller();
    }

    /* Saved for future possible implementation
     *
     * function colors() {
        return $this->belongsToMany('App\Color', 'colors_products', 'product_id', 'color_id');
    }

        function offers(){
        return $this->hasMany('App\ProductOffer')->with('user');
    }
    */

    function sizes() {
        return $this->belongsTo('App\ProductSize', 'size');
    }

    function countries() {
        return $this->belongsTo('App\Country', 'country');
    }

    function genders() {
        return $this->belongsTo('App\Gender', 'gender');
    }

    function views() {
        return $this->hasMany('App\ProductView');
    }

    function reviewByBuyer() {
        return $this->hasOne('App\ReviewIndicator')->where('is_seller', 0);
    }

    function reviewBySeller() {
        return $this->hasOne('App\ReviewIndicator')->where('is_seller', 1);
    }

    function purchase() {
        return $this->hasOne('App\Purchase', 'product_id')->with('user');
    }




    function offersByCurrent(){
        $userId = Auth::check() ? Auth::user()->id : null;
        return $this->hasMany('App\ProductOffer')->where('user_id' , $userId);
    }

    /* function acceptedOffer(){
        $userId = Auth::check() ? Auth::user()->id : null;
        return $this->hasOne('App\ProductOffer')->where('user_id', $userId)->where('accepted', 1)->where('updated_at', '>', Carbon::now()->subWeek());
    } */

    public function getHasLikedAttribute()
    {
        if(Auth::check()){
            return $this->likedBy->count() > 0;
        }else{
            return false;
        }
    }

    public function getCommentCountAttribute()
    {
        return $this->commentCount()->count();
    }

    public function getLikeCountAttribute()
    {
        return $this->likes()->count();
    }

    public function getViewsAttribute()
    {
        return $this->views()->count();
    }

    public function setSlugAttribute($slug)
    {
        $count = Product::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
        $this->attributes['slug'] =  $count ? "{$slug}-{$count}" : $slug;
    }

    public function getImage($i)
    {
        $i = (int) $i - 1;
        $images = $this->images()->get();
        if(isset($images[$i])){
            return $images[$i];
        }else{
            return null;
        }
    }

    public function belongsToCurrentUser(){
        if($user = Auth::user()){
            if($user->id == $this->seller){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}
