<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 10:37
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $appends = ['time_ago'];
    protected $with = ['product', 'user'];

    /**
     * Morph notification type, to a notification.
     */
    public function notification ()
    {
        return $this->morphMany('Notification', 'notification');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function seller()
    {
        return $this->belongsTo('App\User', 'seller_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * Short human readable time
     *
     * @return string
     */
    public function getTimeAgoAttribute()
    {
        $timeAgo = $this->created_at->diffForHumans();

        if (strpos($timeAgo, 'second') !== false || strpos($timeAgo, 'minute') !== false) {
            return '<1h';
        }

        $timeAgo = str_replace(' ago', '', $timeAgo);
        $timeAgo = str_replace([' days', ' day'], 'd', $timeAgo);
        $timeAgo = str_replace([' months', ' month'], 'm', $timeAgo);
        $timeAgo = str_replace([' week', ' weeks'], 'w', $timeAgo);
        $timeAgo = str_replace([' years', ' year'], 'y', $timeAgo);
        $timeAgo = str_replace([' hours', ' hour'], 'h', $timeAgo);

        return $timeAgo;
    }
}