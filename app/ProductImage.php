<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 01/03/16
 * Time: 16:08
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';

    public function src(){
        if($this->base_url){
            $src = $this->base_url.'/'.$this->public_url;
            return $src;
        }else{
            return $this->base_url;
        }
    }

    public function small(){
        if($this->base_url){
            $src = $this->base_url.'/'.$this->public_url;
            return str_replace('original', 'small', $src);
        }else{
            return $this->base_url;
        }
    }

    public function medium(){
        if($this->base_url){
            $src = $this->base_url.'/'.$this->public_url;
            return str_replace('original', 'medium', $src);
        }else{
            return $this->base_url;
        }
    }

    public function large(){
        if($this->base_url){
            $src = $this->base_url.'/'.$this->public_url;
            return str_replace('original', 'large', $src);
        }else{
            return $this->base_url;
        }
    }

    public function xl(){
        if($this->base_url){
            $src = $this->base_url.'/'.$this->public_url;
            return str_replace('original', 'xl', $src);
        }else{
            return $this->base_url;
        }
    }

}