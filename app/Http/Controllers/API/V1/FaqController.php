<?php

namespace App\Http\Controllers\API\V1;

use App\Faq;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class FaqController extends ApiController
{
    public function viewInfo () {
        $faq = Faq::all()->groupBy('subject');
        return parent::api_response($faq, true, ['return' => 'FAQs'], 200);
    }

}
