<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 12:16
 */

namespace App\Http\Controllers\API\V1;


use App\Product;
use App\ProductImage;
use App\StripeUser;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
//use Intervention\Image\ImageManagerStatic as Image;

class ListingController extends ApiController
{

    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';


    public function viewAll () {
        $products = Product::with('type_info', 'countries', 'genders', 'categ_info', 'subcateg_info', 'color', 'sizes', 'comments', 'likes', 'images', 'seller', 'likedBy')->orderBy('created_at', 'desc')->where('seller', Auth::user()->id)->paginate(10);
        return parent::api_response($products, true, ['success' => 'Users Listings'], 200);
    }

    protected function upload(){
        $info = Input::only('name', 'description', 'cost', 'type', 'delivery', 'size',  'gender', 'categ', 'subcateg', 'country');

        /*
         * TODO: Stripe after Profile
         * if (!Auth::user()->stripe_connected) {
            return parent::api_response([], false, ['error' => 'Please enter bank details to upload a product.'], 400);
        }*/

        $product = new Product;
        $product->seller = Auth::user()->id;
        $product->slug = str_slug($info['name'] . ' ' . str_random(10));
        foreach($info as $field => $val){
            $product->$field = $val;
        }
        try{
            $images = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
            if($images['image_1']){
                $product->save();
            }else{
                return parent::api_response([], false, ['error' => 'Please upload at least one image'], 400);
            }
            foreach($images as $index => $image){
                if($image) {
                    $file = $image;
                    $newName = 'product_upload_'.$product->id.'_'.md5(time().$index).'.'.$file->getClientOriginalExtension();
                    $folder = 'user_'.Auth::user()->id;
                    $file->move($this->uploadsFolder.$folder, $newName);
                    Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                    $image = new ProductImage;
                    $image->product_id = $product->id;
                    $image->url = $this->uploadsFolder.$folder.'/'.$newName;
                    $image->public_url = $this->imageCacheRoute.$folder.'/'.$newName;
                    $image->base_url = env('BASE_URL');
                    $image->original_filename = $file->getClientOriginalName();
                    $image->raw_data = $file;
                    /* Future raw data possible usage, needs column modified
                        $stream = fopen($this->uploadsFolder.$folder.'/'.$newName,"r");
                        $image->raw_data = stream_get_contents($stream);
                        fclose($stream);
                    */
                    try{
                        $image->save();
                    }catch (QueryException $e){
                        return parent::api_response($e->errorInfo, false, ['error' => 'image upload failed'], 500);
                    }
                }
            }

//            dd(Color::find(Input::get('color')));
            /*$colors = Input::get('colors');
            if($colors){
                $colors = json_decode($colors);
                foreach($colors as $color){
                    $product->colors()->attach($color);
                }
            }*/

            $new_product = Product::where('id', '=', $product->id)->get();

            return parent::api_response($new_product, true, ['success' => 'product uploaded'], 200);
        }catch (QueryException $e){
                return parent::api_response($e->errorInfo, false, ['error' => 'product upload failed'], 500);
        }
    }

    protected function removeImages(){
        $imageIDs = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
        $productID = Input::get('product_id');
        $product = Product::find($productID)->load('images');

        foreach($imageIDs as $i => $imageID) {
            if (!is_null(ProductImage::find($imageID))) {
                ProductImage::find($imageID)->delete();
//                $img->destroy();
            }
        }

        return parent::api_response($product, true, ['success' => 'Successfully Deleted Product Image'], 200);

    }

    protected function edit(){

        $info = Input::only('id', 'name', 'description', 'cost', 'type', 'delivery', 'size', 'visibility', 'gender', 'categ', 'subcateg', 'country');

        $product = Product::find($info['id']);
        foreach($info as $field => $val){
            $product->$field = $val;
        }
        try{
            $product->save();
            //Wipe deleted images
            $imageIDs = Input::get('deleted_images');
            if($imageIDs){
                $imageIDs = json_decode($imageIDs);
                foreach($imageIDs as $i => $imageID) {
                    if (!is_null(ProductImage::find($imageID))) {
                        $image = ProductImage::find($imageID);
                        File::delete($image->url);
                        $image->delete();
                    }
                }
            }

            $images = Input::only('image_1', 'image_2', 'image_3', 'image_4', 'image_5');
            

            //TODO: Set up correct file sizes and URL's
            if($images){
                foreach($images as $index => $image){
                    if($image){
                        $file = $image;
                        $newName = 'product_upload_'.$product->id.'_'.md5(time().$index).'.'.$file->getClientOriginalExtension();
                        $folder = 'user_'.Auth::user()->id;
                        $file->move($this->uploadsFolder.$folder, $newName);
                        Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                        $image = new ProductImage;
                        $image->product_id = $product->id;
                        $image->url = $this->uploadsFolder.$folder.'/'.$newName;
                        $image->public_url = $this->imageCacheRoute.$folder.'/'.$newName;
                        $image->base_url = env('BASE_URL');
                        $image->original_filename = $file->getClientOriginalName();
                        try{
                            $image->save();
                        }catch (QueryException $e){
                            return parent::api_response($e->errorInfo, false, ['error' => 'image upload failed'], 500);
                        }
                    }
                }
            }

            return parent::api_response($product, true, ['success' => 'product edited'], 200);
        }catch (QueryException $e){
            return parent::api_response($e->errorInfo, false, ['error' => 'product editing failed'], 500);
        }
    }

    public function addViews(){
        $data = Input::only('products');
        $data = json_decode($data['products']);

        foreach ($data as $productId) {
            $product = Product::find($productId);
            if($product){
                $view = New ProductView;
                $view->user_id = Auth::user()->id;
                $view->product_id = $productId;
                if(!$view->save()){
                    return parent::api_response([], false, ['error' => 'adding views failed'], 500);
                }
            }else{
                return parent::api_response([], false, ['error' => 'product not found'], 404);
            }
        }
        return parent::api_response([], true, ['success' => 'views saved'], 200);
    }

    /**
     * Mark a product as sold
     * @param $id
     * @return mixed
     */
    protected function markSold($id){
        $product = Product::where('id', $id)->where('seller', Auth::user()->id )->first();
        if($product){
            $product->marked_as_sold = 1;
            $product->sold = 1;
            if($product->save()){
                return parent::api_response($product, true, ['success' => 'product marked sold'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'mark sold failed'], 500);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

    protected function markAvailable($id){
        $product = Product::where('id', $id)->where('seller', Auth::user()->id )->first();
        if($product){
            $product->marked_as_sold = 0;
            $product->sold = 0;
            if($product->save()){
                return parent::api_response($product, true, ['success' => 'product marked available'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'mark available failed'], 500);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

//    SEEDER
    function seedImages(){
        $products = Product::get();
        foreach($products as $product){
            $image = new ProductImage;
            $image->product_id = $product->id;
            $image->url ='uploads/users/test/'.mt_rand(1,5).'.jpg';
            $image->public_url ='images/original/users/test/'.mt_rand(1,5).'.jpg';
            $image->base_url = 'http://mmmp.dreamrserve.uk';
            $image->original_filename = 'test_image.jpg';
            $image->save();
        }

        return 'SEEDED';
    }

}