<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 18/01/16
 * Time: 16:51
 */

namespace App\Http\Controllers\API\V1;

use App\User;
use App\Categ;
use App\Subcateg;
use App\Gender;
use App\Type;
use App\ProductSize;
use App\PriceRanges;
use App\Country;
use App\UserSearchable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Facebook\Authentication\AccessToken;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookRequest;


class AuthController extends ApiController

{
    use AuthenticatesUsers;

    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;
    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';

    /**
     * Obtain the user information from Facebook and login/register user.
     */
    public function fbLogin(Request $request)
    {
        $fb = new Facebook([
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'default_graph_version' => 'v2.2',
        ]);


        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,email,picture', Input::get('access_token'));
        } catch(FacebookResponseException $e) {
            return parent::api_response([], false, ['error' => $e->getMessage()], 500);
        } catch(FacebookSDKException $e) {
            return parent::api_response([], false, ['error' => $e->getMessage()], 500);
        }


        $fb_user = $response->getGraphUser();



        if($exists = UserSearchable::where('facebook_id', $fb_user->getId())->first()){
            $token = JWTAuth::fromUser($exists);
            return $this->api_response(['token' =>$token, 'user' => $exists] , true, ['Success' => 'logged in'], 200);
        }else{
            try {
                if ($non_fb = UserSearchable::where('email', $fb_user->getEmail())->first()) {
                    $non_fb->facebook_id = $fb_user->getId();
                    $non_fb->save();

                    $token = JWTAuth::fromUser($non_fb);

                    return $this->api_response(['token' => $token, 'user' => $non_fb], true, ['Success' => 'logged in'], 200);
                } else {

                    $validator = Validator::make($request->all(), [
                        'email' => 'required|email|unique:users|max:255',
                        'username' => 'required|min:4|max:36|unique:users',
                        'city' => 'required',
                        'name' => 'required',
                        'country' => 'required',
                    ]);

                    if ($validator->fails()) {
                        //missing data
                        $fb_data = [
                            'new_user' => true,
                            'email' => $fb_user->getEmail(),
                            'name' => $fb_user->getName(),
                            'image' => 'https://graph.facebook.com/v2.6/' . $fb_user->getId() . '/picture?width=1920',
                        ];
                        return parent::api_response($fb_data, false, $validator->errors()->first(), 401);
                    } else {
                        $user = new User;
                        $user->facebook_id = $fb_user->getId();
                        $user->name = Input::get('name');
                        $user->username = Input::get('username');
                        $user->email = Input::get('email');
                        $user->password = bcrypt(str_random(12));
                        $user->city = Input::get('city');
                        $user->bio = '-';
                        $user->country = Input::get('country');
                        $user->image = 'https://graph.facebook.com/v2.6/' . $fb_user->getId() . '/picture?width=1920';
                        $user->save();

                        $token = JWTAuth::fromUser($user);

                        //Get full user object from DB
                        $user = UserSearchable::find($user->id);

                        return $this->api_response(['token' => $token, 'user' => $user], true, ['Success' => 'logged in'], 200);
                    }
                }
            }catch (\Exception $e){
                return parent::api_response([], false, ['error' => $e->getMessage()], 500);
            }

        }
    }

    /**
     * Log in a user.
     *
     * @param Request $request
     * @return json
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        //Limit the amount of times users can login
        if ($this->hasTooManyLoginAttempts($request)) {
            return parent::api_response([], 'too many authentication attempts, try again in '.$this->lockoutTime.' seconds', false, 401);
        }

        $token = JWTAuth::attempt($credentials);

        //If the login attempt failed
        if (!$token) {
            $this->incrementLoginAttempts($request);
            return parent::api_response([], 'invalid credentials', 401);
        }

        $user = User::findOrFail(Auth::user()->id);

        return parent::api_response([
            'token' => $token,
            'user' => $user
        ]);
    }

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $data = Input::only('email', 'password', 'username', 'name', 'city', 'county', 'country', 'image');
        //return parent::api_response($data['image'], true, ['Success' => 'Appdata'], 200);

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|min:8',
            'username' => 'required|min:4|max:36|unique:users',
            'city' => 'required',
            'country' => 'required',
            'image' => 'image'
        ]);
        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
        } else {
            $new_user = new User;
            $new_user->email = $data['email'];
            $new_user->password = bcrypt($data['password']);
            $new_user->username = $data['username'];
            $new_user->city = $data['city'];
            $new_user->name = $data['name'];
            $new_user->country = $data['country'];
            if(!empty($data['bio'])){
                $new_user->bio = $data['bio'];
            } else {
                $new_user->bio = '-';
            }

            // upload profile image and delete the old one IF exists
            /*$file = $data['image'];
            $autoIncrement = DB::select(DB::raw('SELECT auto_increment FROM information_schema.tables WHERE table_schema=\''.getenv('DB_DATABASE').'\' and table_name=\'users\';'));
            if(strstr($file, 'jpeg')) {
                //$newName = 'user_profile_'.$autoIncrement[0]->auto_increment.'_'.md5(time()).'.'.$file->getClientOriginalExtension();
                $newName = 'user_profile_' . $autoIncrement[0]->auto_increment . '_' . md5(time()) . '.jpeg';
            } elseif(strstr($file, 'png')) {
                $newName = 'user_profile_' . $autoIncrement[0]->auto_increment . '_' . md5(time()) . '.png';
            } else {
                $newName = 'user_profile_' . $autoIncrement[0]->auto_increment . '_' . md5(time()) . '.jpeg';
            }
            $decoded = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file));
            $folder = 'uploads/users/user_'.$autoIncrement[0]->auto_increment;

            //$temp_file = $data['username'];
            if(!is_dir("/$folder")) mkdir($folder);
            file_put_contents($folder.'/'.$newName, $decoded);
            //file->move($this->uploadsFolder.$folder, $newName);
            //Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();*/

            // upload profile image and delete the old one IF exists

            $autoIncrement = DB::select(DB::raw('SELECT auto_increment FROM information_schema.tables WHERE table_schema=\''.getenv('DB_DATABASE').'\' and table_name=\'users\';'));

            $file = $data['image'];
            $newName = 'user_profile_'.$autoIncrement[0]->auto_increment.'_'.md5(time()).'.'.$file->getClientOriginalExtension();
            $folder = 'user_'.$autoIncrement[0]->auto_increment;
            $file->move($this->uploadsFolder.$folder, $newName);
            Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
            $new_user->image = env('BASE_URL').'/'.$this->uploadsFolder.$folder.'/'.$newName;

            if($new_user->save()) {

                $request = new Request();
                $request->merge($data);
                return $this->login($request);
            }
        }
    }

    /**
     * Logout user
     * @return mixed
     */
    public function logout(){
        if(!Auth::logout()){
            return $this->api_response([], true, ['Success' => 'logged out'], 200);
        }
    }

    /**
     * Reset password
     * @return mixed
     */
    public function reset(){
        $email = stripslashes(Input::get('email'));
        $user = User::where('email', $email)->first();

        if($user){
            $raw_pass = str_random(12);
            $new_pass = bcrypt($raw_pass);
            $user->password = $new_pass;
            $data['user'] = $user->toArray();
            $data['password'] = $raw_pass;
            if($user->parse_id && !$user->initial_pass_reset){
                $user->initial_pass_reset = 1;
            }
            if($user->save()){
                Mail::send('emails.API.V1.reset', ['data' => $data], function ($m) use ($email, $data) {
                    $m->from('hey@MMMarket-test.com', 'My Mini Marketplace');

                    $m->to($email, 'user')->subject('Pass Reset');
                });
                return parent::api_response([], true, ['Success' => 'new pass sent'], 200);
            }

        }else{
            return parent::api_response([], false, ['Error' => 'user not found'], 404);
        }


    }

    public function edit(Request $request){
        $data = Input::only('email', 'password');
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'email' => 'email|unique:users|max:255',
        ]);
        if ($validator->fails()) {
            return parent::api_response($validator->errors(), false, ['Error' => 'email invalid/used'], 401);
        } else {
            if($data['email']){
                $user->email = stripslashes($data['email']);
            }
            if($data['password']){
                $user->password = bcrypt(stripslashes($data['password']));
            }

            if($user->save()){
                return parent::api_response([], true, ['Success' => 'new details saved'], 200);
            }
        }
    }

    public function appdata () {
        $categs = Categ::with('subcategories')->orderby('name')->get();
        //$subcategs = Subcateg::orderby('name')->get();
        $productSizes = ProductSize::all();
        $types = Type::all();
        $genders = Gender::all();
        $countries = Country::all();
        $priceRanges = PriceRanges::all();

        $data = [
            'categs' => $categs,
            //'subcategs' => $subcategs,
            'product_sizes' => $productSizes,
            'types' => $types,
            'genders' => $genders,
            'countries' => $countries,
            'price_ranges' => $priceRanges
        ];

        return parent::api_response($data, true, ['Success' => 'Appdata'], 200);
    }

    public function update(Request $request)
    {
        $appVersion = config('myminimarketplace.version.ios');

        if ($appVersion == null) {
            return parent::api_response([], false, ['error' => 'invalid app parameter'], 200);
        }

        $shouldUpdate = version_compare($appVersion, $request->input('version'), '>');

        $data = [
            'should_update' => $shouldUpdate
        ];

        return parent::api_response($data, true, ['Success' => 'update'], 200);
    }


}