<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 24/02/16
 * Time: 16:35
 */

namespace App\Http\Controllers\API\V1;


use App\Comment;
use App\Follow;
use App\ReportedProduct;
use App\Product;
use App\ProductLike;
use App\PriceRanges;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ProductController extends ApiController
{
    function viewAll(){

        $products = Product::with('type_info', 'countries', 'genders', 'categ_info', 'subcateg_info', 'sizes', 'images', 'seller', 'likedBy')
            ->with(['seller' => function($query) {
                    $query->with('follows','followers');
                    }
                ])
            ->where('seller', '!=' , Auth::user()->id)
            //->with('views', 'categ_info', 'colors', 'sizes', 'images', 'seller', 'likedBy')
            ->where('sold', 0)
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return parent::api_response($products, true, ['return' => 'all products'], 200);
    }

    /**
     * Main feed endpoint for seeing all products by followed users
     * @return mixed
     */
    function feed(){
        $following = Follow::where('user_id', Auth::user()->id)->get();
        $ids = $following->pluck('follows');

        //Admin account
        //$ids->push(env('GEORGE_ID', 1));

        // Implementing keeping products pending purchase still shown in Feed until they get paid

        $purchaseIds = DB::table('purchases')
            ->select(DB::raw('id'))
            ->where('buyer_id', Auth::user()->id)
            ->where('status', 'pending')
            ->groupBy('id')
            ->pluck('id');

        $products = Product
            ::where('seller', '!=' , Auth::user()->id)
            ->whereIn('seller', $ids)
            ->with('type_info', 'countries', 'genders', 'categ_info', 'subcateg_info', 'images', 'seller', 'seller.followers', 'seller.follows', 'likedBy', 'comments')
            ->leftJoin('purchases', 'products.id','=', 'purchases.product_id')
            ->select('products.*')
            ->where('products.sold', 0)
            ->orWhereIn('purchases.id', $purchaseIds)
            ->orderBy('products.created_at', 'DESC')
            ->paginate(20);

        $products = $products->toArray();

        return parent::api_response($products, true, ['return' => 'all products'], 200);
    }

    function get($id){
        //$product = Product::with('categ_info', 'colors', 'sizes', 'comments', 'images', 'seller', 'likedBy')->where('id', $id)->paginate(1);
        //$product = Product::with('sizes', 'seller')->where('id', $id)->paginate(1);
        $product = Product::find($id);
        if($product) {
            return parent::api_response($product, true, ['return' => 'selected product'], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

    function getLikes($id){
        $likes = ProductLike::where("product_id", $id)->with('user')->get();
        $liked_by = ProductLike::where('user_id', Auth::user()->id)->where('product_id', $id)->first();
        if($liked_by){
            $liked_by = true;
        }else{
            $liked_by = false;
        }
        return parent::api_response($likes, true, ['return' => 'likes for selected product', 'liked_by' => $liked_by], 200);
    }

    public function search(){
        $filters = Input::only('gender', 'categs', 'subcategs', 'sizes', 'price', 'price_range', 'conditions', 'term', 'country');
        $sort = Input::get('sort');

        $products = Product::where('seller', '!=' , Auth::user()->id)
            ->with('type_info', 'genders', 'countries', 'categ_info', 'subcateg_info', 'sizes', 'comments', 'images', 'seller', 'likedBy')
            ->with(['seller' => function($query) {
                $query->with('follows','followers');
            }
            ]);
            if (isset($filters['categs'])) {
                $products->whereHas('categ_info', function ($query) use ($filters) {
                    if (isset($filters['categs'])) {
                        $query->whereIn('id', json_decode($filters['categs'],true));
                    }
                });
            }
            if (isset($filters['subcategs'])) {
                $products->whereHas('subcateg_info', function ($query) use ($filters) {
                    if (isset($filters['subcategs'])) {
                        $query->whereIn('id', json_decode($filters['subcategs'],true));
                    }
                });
            }
            if (isset($filters['sizes'])) {
                $products->whereHas('sizes', function ($query) use ($filters) {
                    if (isset($filters['sizes'])) {
                        $query->whereIn('id', $filters['sizes']);
                    }
                });
            }
            if (isset($filters['country'])) {
                $products->whereHas('countries', function ($query) use ($filters) {
                    if (isset($filters['country'])) {
                        $query->whereIn('id', $filters['country']);
                    }
                });
            }
            if(isset($filters['conditions'])){
                //$products->where('type', '=', $filters['type']);
                $products->whereHas('type_info', function ($query) use ($filters) {
                    if (isset($filters['conditions'])) {
                        $query->whereIn('id', json_decode($filters['conditions'],true));
                    }
                });
            }
            if(isset($filters['gender'])){
                $products->where('gender', '=', $filters['gender']);
            }
            if(isset($filters['price'])){
                $prices = json_decode($filters['price'],true);
                if(count($prices) > 1){
                    $products->whereBetween('cost', $prices);
                }else{
                    $products->where('cost', '>=', array_values($prices)[0]);
                }
            }
            if(isset($filters['price_range'])){
                $price_ranges = PriceRanges::find($filters['price_range']);
                $price_ranges = explode('-',$price_ranges->name);
                if(count($price_ranges) > 1){
                    $products->whereBetween('cost', $price_ranges);
                }else{
                    $price_ranges[] = '65534';
                    $products->whereBetween('cost', $price_ranges);
                }
            }

        // Implementing keeping products pending purchase still shown in Search results until they get paid

        $purchaseIds = DB::table('purchases')
            ->select(DB::raw('id'))
            ->where('buyer_id', Auth::user()->id)
            ->where('status', 'pending')
            ->groupBy('id')
            ->pluck('id');

        //$products->where('sold', 0);

        $products->leftJoin('purchases', 'products.id','=', 'purchases.product_id')
            ->select('products.*')
            ->where('products.sold', 0)
            ->orWhereIn('purchases.id', $purchaseIds);

        if(isset($filters['term'])){
            $products->search($filters['term']);
        }

        if (empty($sort)) {
            $sort = 'recent';
        }

        switch ($sort){
            case 'price_low_high':
                if(!$filters['term']){
                    $products->orderBy('cost', 'asc');
                }else{
                    $query = $products->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'ASC']);
                    $products = $products->setQuery($query);
                }

                break;
            case 'price_high_low':
                if(!$filters['term']){
                    $products->orderBy('cost', 'desc');
                }else{
                    $query = $products->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'DESC']);
                    $products = $products->setQuery($query);
                }
                break;
            case 'recent':
                if(!$filters['term']){
                    $products->orderBy('created_at', 'desc');
                }else{
                    $query = $products->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'created_at', 'direction' => 'DESC']);
                    $products = $products->setQuery($query);
                }
                break;
        }

        if(isset($filters['term'])){
            $filters['term'] = ' '.$filters['term'];
        }

        return parent::api_response($products->paginate(20), true, ['return' => 'Search Results'.$filters['term']], 200);
    }

    public function like($id){
        $product = Product::whereSold(0)->find($id);
        if($product){
            $exists = ProductLike::where('product_id', $id)->where('user_id', Auth::user()->id)->get();
            if(!$exists->count()){
                $like = new ProductLike;
                $like->product_id = $id;
                $like->user_id = Auth::user()->id;
                if($like->save()){
                    if($like->user_id != $product->seller){
                        $pushData = [
                            'notification_id' => $like->id,
                            'notification_type' => 'ProductLike',
                            'channel' => 'user_'.$product->seller,
                            'recipient' => $product->seller,
                            'message' => Auth::user()->name.' liked '.$product->name,
                            'snippet' => $product->name,
                            'extra_id' => $product->id,
                        ];
                        $push = new NotificationController;
                        $push->sendNotification($pushData);
                    }
                    return parent::api_response($like, true, ['success' => 'Product '.$id.' liked'], 200);
                }
            }else{
                return parent::api_response($exists, true, ['error' => 'You\'ve already liked that product'], 400);
            }

        }else{
            return parent::api_response([$id], true, ['error' => 'Product not found'], 404);
        }
    }

    public function unlike($id)
    {
        $film = Product::whereSold(0)->find($id);
        if ($film) {
            $exists = ProductLike::where('product_id', $id)->where('user_id', Auth::user()->id)->first();
            if ($exists) {
                if($exists->count()) {
                    if ($exists->delete()) {
                        return parent::api_response($exists, true, ['success' => 'Product ' . $id . ' unliked'], 200);
                    } else {
                        return parent::api_response($exists, true, ['error' => 'Error deleting product'], 500);
                    }
                }
            } else {
                return parent::api_response([$id], true, ['error' => 'You have not liked that product'], 400);
            }
        }else {
            return parent::api_response([$id], true, ['error' => 'Product not found'], 404);
        }
    }

    function reportProduct($id)
    {
        $product = Product::whereSold(0)->find($id);
        $exists = ReportedProduct::where('product_id', $id)->where('user_id', Auth::user()->id)->first();
        if($product && !$exists){
            $report = New ReportedProduct;
            $report->user_id = Auth::user()->id;
            $report->product_id = $id;
            if($report->save()){
                /*$count = ReportedProduct::where('product_id', $id)->get();
                if($count->count() > 50){
                  //TODO: Add logic to remove product once method is decided upon
                }*/
                return parent::api_response($report, true, ['success' => 'Reported product with id '.$id], 200);
            } else {
                return parent::api_response([], true, ['error' => 'Error reporting product'], 500);
            }
        }else {
            return parent::api_response([$id], true, ['error' => 'Can\'t report that product'], 404);
        }
    }




}