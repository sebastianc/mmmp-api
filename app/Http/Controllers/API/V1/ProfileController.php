<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 17:03
 */

namespace App\Http\Controllers\API\V1;

use App\BlockedUser;
use App\Follow;
use App\Purchase;
use App\Review;
use App\Product;
use App\ProductImage;
use App\ProductLike;
use App\User;
use App\UserSearchable;
//use Auth;
//use GuzzleHttp\Message\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProfileController extends ApiController
{

    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';

    function viewProducts(){
        $products = Product::where('seller', Auth::user()->id)->where('sold', 0)->with('type_info', 'countries', 'genders', 'categ_info', 'subcateg_info', 'sizes', 'comments', 'likes', 'images', 'seller', 'seller.followers', 'seller.follows')->orderBy('created_at', 'desc')->paginate(10);
        
        return parent::api_response($products, true, ['return' => 'products by this authenticated user'], 200);
    }

    function viewSoldProducts(){
        $products = Product
            ::with('reviewBySeller', 'reviewByBuyer')
            ->where('seller', Auth::user()->id)
            ->where('sold', 1)
            ->with('type_info', 'countries', 'genders', 'categ_info', 'subcateg_info', 'sizes', 'comments', 'likes', 'images', 'seller', 'reviewByBuyer', 'reviewBySeller', 'purchase')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return parent::api_response($products, true, ['return' => 'sold products by this authenticated user'], 200);
    }

    function viewPurchased(){
//        $purchases = Purchase::with('product')->where('buyer_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(10);
        $purchaseIds = DB::table('purchases')
            ->select(DB::raw('id'))
            ->where('buyer_id', Auth::user()->id)
            ->where('payment_sent', 1)
            ->groupBy('id')
            ->pluck('id');

        $purchases = Product::with('purchase', 'reviewBySeller', 'reviewByBuyer')
            ->where('payment_sent', 1)
            ->leftJoin('purchases', 'products.id','=', 'purchases.product_id')
            ->whereIn('purchases.id', $purchaseIds)
            ->select('products.*')
            ->orderBy(DB::raw('purchases.created_at'), 'desc')->paginate(10);

        return parent::api_response($purchases, true, ['return' => 'products purchased by this authenticated user'], 200);

    }

    function buildProfile(){
        //TODO: Remove extra queries and leave user only
        $user = UserSearchable::with('follows', 'followers')->find(Auth::user()->id);
        //$follow = Follow::where('user_id', $user->id)->get();
        $followers = Follow::where('follows', $user->id)->get();
        $reviews_seller = Review::where('seller_id', $user->id)->where('is_seller', 1)->get();
        $reviews_buyer = Review::where('seller_id', $user->id)->where('is_seller', 0)->get();
        

        if (isset($reviews_seller)) {
            $user['reviews_as_seller'] = $reviews_seller;
        }
        if (isset($reviews_buyer)) {
            $user['reviews_as_buyer'] = $reviews_buyer;
        }
        /*if (isset($follow)) {
            $user['following'] = $follow;
        }*/
        if (isset($followers)) {
            $user['followers'] = $followers;
        }
        $user['phone_number'] = $user->phone();
        $user['user_email'] = $user->getEmail();

        return $user;
    }

    public function viewInfo(){
        return parent::api_response($this->buildProfile(), true, ['return' => 'user info'], 200);
    }

    function reviewsAsBuyer(){
        $reviews = Review::with('user')
            ->where('user_id', Auth::user()->id)
            ->where('is_seller', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return parent::api_response($reviews, true, ['return' => 'reviews as buyer'], 200);
    }

    function reviewsAsSeller(){
        $reviews = Review::with('user')
            ->where('seller_id', Auth::user()->id)
            ->where('is_seller', 0)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return parent::api_response($reviews, true, ['return' => 'reviews as seller'], 200);
    }

    function followers(){
//        $followers = Follow::where('follows', Auth::user()->id)->with('users')->paginate(10);

        $followers = UserSearchable::whereHas('follows', function($q){
            $q->where('follows', Auth::user()->id);
        })->paginate(10);
        return parent::api_response($followers, true, ['return' => 'followers'], 200);

    }

    function following(){
//        $followers = Follow::where('user_id', Auth::user()->id)->with('follows')->paginate(10);
        $followers = UserSearchable::whereHas('followers', function($q) {
            $q->where('user_id', Auth::user()->id);
        })->paginate(10);
        return parent::api_response($followers, true, ['return' => 'following'], 200);

    }

    function likes($id){
        $likes = ProductLike::where('user_id', $id)->with('product')->paginate(10);
        return parent::api_response($likes, true, ['return' => 'products liked by user '.$id], 200);

    }

    protected function delete($id){
        $product = Product::where('id', $id)->where('seller', Auth::user()->id )->first();
        if($product){
            if($product->delete()){
                return parent::api_response($product, true, ['success' => 'product deleted'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'delete failed'], 500);
            }
        }else{
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

    function editProfile() {
        $data = Input::only('bio', 'city', 'country', 'image', 'name', 'phone', 'email');

        $user = User::where('id', Auth::user()->id)->first();

        if ($user) {

            $validator = Validator::make($data, [
                'bio' => 'string',
                'city' => 'string',
                'country' => 'string',
                'name' => 'string',
                //'image' => 'image',
                //'phone' => 'string'
            ]);

            if ($validator->fails()) {
                return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
            } else {
                // Only update non-null keys
                foreach ($data as $k => $v) {
                    if (!is_null($v)) {
                        if ($k == 'image') {
                            // upload profile image and delete the old one IF exists
                            $file = $v;
                            $newName = 'user_profile_'.Auth::user()->id.'_'.md5(time()).'.'.$file->getClientOriginalExtension();
                            $folder = 'user_'.Auth::user()->id;
                            $file->move($this->uploadsFolder.$folder, $newName);
                            Image::make($this->uploadsFolder.$folder.'/'.$newName)->save();
                            $user[$k] = env('BASE_URL').'/'.$this->uploadsFolder.$folder.'/'.$newName;
                        } else {
                            if($k == 'phone'){
                                $user->phone_verified = false;
                                $user->phone = urlencode($v);
                            } else {
                                $user[$k] = $v;
                            }
                        }
                    }
                }

                //$user->password = bcrypt($data['password']);

                if ($user->save()) {
                    $updated = $this->buildProfile();
                    return parent::api_response($updated, true, ['return' => 'updated profile successfully'], 200);
                } else {
                    return parent::api_response(null, true, ['return' => 'problem updating profile'], 500);
                }
            }
        }
        return parent::api_response(null, true, ['return' => 'problem updating profile, your user was deleted after you logged in'], 500);
    }

    /**
     * Report a user and add them to warned list
     *
     * @param string $slug The users username
     * @return \Illuminate\Http\RedirectResponse
     */
    function report($id)
    {
        $user = User::where('id', $id)->first();

        if (!$user) {
            return parent::api_response([], false, 'Could not report user.', 500);
        }

        if (BlockedUser::currentUserHasBlocked($user->id)->count() != 0) {
            return parent::api_response([], false, 'Could not report user.', 500);
        }

        $block = new BlockedUser();
        $block->user_id = Auth::user()->id;
        $block->blocked = $user->id;
        $block->save();

        $blockCount = BlockedUser::where('blocked', $user->id)->count();
        if ($blockCount > 15) {
            $user->delete();
        }

        return parent::api_response([], true, 'User reported.', 200);
    }
}

