<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 10:13
 */

namespace App\Http\Controllers\API\V1;


use App\BlockedUser;
use App\Follow;
use App\Review;
use App\Product;
use App\ProductLike;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserSearchable;
use Illuminate\Support\Facades\Input;

class UserController extends ApiController
{

    public function getProducts($id) {

        $products = Product::where('seller', $id)->where('sold', 0)->with('type_info', 'countries', 'genders', 'categ_info', 'subcateg_info', 'sizes', 'comments', 'likes', 'likedBy', 'images', 'seller')->orderBy('created_at', 'desc')->paginate(20);

        return parent::api_response($products, true, ['return' => 'products by user with id '.$id], 200);
    }

    public function getUsers() {

        $filters = Input::only('term');

        $myId = Auth::user()->id;
        if(isset($filters['term'])){
            $users = UserSearchable::where('id', '!=', Auth::user()->id)->with('follows', 'followers', 'following')->search($filters['term'])->orderBy('created_at', 'desc')->paginate(30);
        } else {
            $users = UserSearchable::where('id', '<>', $myId)->with('follows', 'followers', 'following')->orderBy('created_at', 'desc')->paginate(30);
        }
        foreach ($users as $user) {
            $is_followed = count(Follow::where('follows', $user->id)->where('user_id', $myId)->with('users')->get());
            $user['is_or_not_followed'] = $is_followed;
        }

        return parent::api_response($users, true, ['return' => 'User search']);
    }

    function getById($id){
        //$user = UserSearchable::with('follows', 'followers')->find($id);
        $user = UserSearchable::find($id);
        if($user){
            //$average_reviews = Review::where('seller_id', $user->id)->avg('rating');
            //$user['average_rating'] = number_format($average_reviews, 1);
            return parent::api_response($user, true, ['return' => 'user '], 200);
        } else {
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    /**
     * Reusable function for removing blocked users from user based requests
     * @param $users
     * @return mixed
     */

    //TODO: Change to more efficient function
    function blocked($users){
        $blocked = BlockedUser::where('user_id', Auth::user()->id)->get()->pluck('blocked');
        $blockedBy = BlockedUser::where('blocked', Auth::user()->id)->get()->pluck('user_id');
        return $users->whereNotIn('id', $blockedBy)->whereNotIn('id', $blocked);
    }

    function follow($id){
        $user = User::where('id', $id);
        //$user = $this->blocked($user)->get();
        if($user->count()){
            $exists = Follow::where('user_id', Auth::user()->id)->where('follows', $id)->get();
            if(!$exists->count()){
                $follow = new Follow;
                $follow->user_id = Auth::user()->id;
                $follow->follows = $id;
                if($follow->save()){
                    $pushData = [
                        'notification_id' => $id,
                        'notification_type' => 'Follow',
                        'channel' => 'user_'.$id,
                        'recipient' => $id,
                        'message' => Auth::user()->name.' is now following you',
                        'extra_id' => $id,
                    ];
                    $push = new NotificationController;
                    $push->sendNotification($pushData);
                    return parent::api_response($follow, true, ['success' => 'followed user '.$id], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'error saving'], 500);
                }
            }else{
                return parent::api_response([], false, ['error' => 'Already liked this user'], 400);
            }
        }else{
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    function unfollow($id){
        $user = User::where('id', $id);
        if($user->count()){
            $exists = Follow::where('user_id', Auth::user()->id)->where('follows', $id)->first();
            if($exists){
                if($exists->delete()){
                    return parent::api_response($exists, true, ['success' => 'unfollowed user '.$id], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'error removing'], 500);
                }
            }else{
                return parent::api_response([], false, ['error' => 'You do not follow that user'], 400);
            }
        }else{
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    function block($id){
        $user = User::find($id);
        if($user){
            $exists = BlockedUser::where('user_id', Auth::user()->id)->where('blocked', $id)->get();
            if(!$exists->count()){
                $block = new BlockedUser;
                $block->user_id = Auth::user()->id;
                $block->blocked = $id;
                if($block->save()){
                    $blocks = BlockedUser::where('blocked', $id)->get();
                    if($blocks->count() > 15){
                        $user = UserSearchable::find($id);
                        if($user){
                            $user->delete();
                        }
                    }
                    return parent::api_response($block, true, ['success' => 'blocked user '.$id], 200);
                }else{
                    return parent::api_response([], false, ['error' => 'block failed'], 500);
                }
            }else{
                return parent::api_response([], true, ['success' => 'Already blocked that user'], 200);
            }
        }else{
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    function getUserRating($id){
        $average_reviews = Review::where('seller_id', $id)->avg('rating');
        return parent::api_response(['average_rating' => number_format($average_reviews, 1)], true, ['return' => 'Average rating'], 200);
    }
    function getLikes($id){
        $products = ProductLike::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(20);
        return parent::api_response($products, true, ['return' => 'likes by user'], 200);
    }
    function getFollowers($id){
//        $followersTest = Follow::where('follows', $id)->with('users')->paginate(10);


        $followers = UserSearchable::with('followers', 'follows')->whereHas('follows', function($q) use ($id){
            $q->where('follows', $id);
        })->paginate(10);

        $myId = Auth::user()->id;
        foreach ($followers as $user) {
            $is_followed = count(Follow::where('follows', $user->id)->where('user_id', $myId)->with('users')->get());
            $user['is_or_not_followed'] = $is_followed;
        }

        return parent::api_response($followers, true, ['return' => 'user followers'], 200);
    }
    function getFollowing($id){
//        $following = Follow::where('user_id', $id)->with('follows')->paginate(10);
        $following = UserSearchable::with('followers', 'follows')->whereHas('followers', function($q) use ($id){
            $q->where('user_id', $id);
        })->paginate(10);

        $myId = Auth::user()->id;
        foreach ($following as $user) {
            $is_followed = count(Follow::where('follows', $user->id)->where('user_id', $myId)->with('users')->get());
            $user['is_or_not_followed'] = $is_followed;
        }

        return parent::api_response($following, true, ['return' => 'user following'], 200);
    }



}