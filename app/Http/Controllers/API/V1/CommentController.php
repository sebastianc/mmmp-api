<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 12:19
 */

namespace App\Http\Controllers\API\V1;


use App\Comment;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CommentController extends ApiController
{
    public function post($id){
        $product = Product::whereSold(0)->find($id);
        $text = stripslashes(Input::get('comment_text'));
        if($product && $text){
            $comment = new Comment;
            $comment->product_id = $id;
            $comment->user_id = Auth::user()->id;
            $comment->text = $text;
            if($comment->save()){
                if($comment->user_id !== $product->seller){
                    $pushData = [
                        'notification_id' => $comment->id,
                        'notification_type' => 'Comment',
                        'channel' => 'user_'.$product->seller,
                        'recipient' => $product->seller,
                        'message' => Auth::user()->name.' commented on '.$product->name,
                        'snippet' => $product->name,
                        'extra_id' => $product->id,

                    ];
                    $push = new NotificationController;
                    $push->sendNotification($pushData);
                }
                return parent::api_response($comment, true, ['success' => 'comment sent'], 200);
            }else{
                return parent::api_response([$id], true, ['error' => 'There was an error posting your comment' ], 500);
            }
        }else{
            return parent::api_response([$id], true, ['error' => 'Please enter a comment and a valid product id'], 400);
        }
    }
    public function get($id){
        $product = Product::find($id);
        if($product){
            $comments = Comment::where('product_id', $id)->with('user')->orderBy('created_at', 'desc')->paginate(10);
            return parent::api_response($comments, true, ['return' => 'product comments'], 200);
        }else{
            return parent::api_response([$id], true, ['error' => 'Product not found'], 404);
        }
    }



}