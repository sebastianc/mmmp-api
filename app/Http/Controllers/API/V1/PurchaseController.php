<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 12:14
 */

namespace App\Http\Controllers\API\V1;


use App\Dispute;
use App\Events\PaymentEvent;
use App\Jobs\SendAdminDisputeEmail;
use App\ManagedStripe;
use App\Purchase;
use App\Product;
use App\ProductImage;
use App\StripeAccount;
use App\StripeUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PurchaseController extends ApiController
{
    /**
     * Pay for a product after pressing buy.
     * @return mixed
     */
    protected function buy(){

        try{
            $purchase_id = Input::get('purchase_id');
            $stripe_token = Input::get('stripe_token');
            $purchase = Purchase::find($purchase_id);
            //If purchase exists
            if($purchase){
                $product = Product::find($purchase->product_id);
                $seller = User::findOrFail($product->seller);
                $buyer = Auth::user();

                //If product hasn't already been paid for
                if(!$purchase->payment_sent) {

                    $stripe = new ManagedStripe();
                    $stripeTransaction = $stripe->payToUser($seller, $purchase->price, $stripe_token, $purchase->id, request()->ip());

                    //If stripe returns error exit payment function
                    if (!$stripeTransaction) {
                        return parent::api_response([] , false, ['error' => 'payment failed'], 500);
                    } else {
                        $purchase->payment_sent = 1;
                        $purchase->status = 'paid';

                        $purchase->billing_name = $buyer->name;
                        $purchase->address_line1 = $stripeTransaction['source']['address_line1'];
                        $purchase->address_line2 = $stripeTransaction['source']['address_line2'];
                        $purchase->address_city = $stripeTransaction['source']['address_city'];
                        $purchase->address_zip = $stripeTransaction['source']['address_state'];
                        $purchase->address_country = $stripeTransaction['source']['address_country'];
                        $purchase->address_state = $stripeTransaction['source']['address_state'];

                        //shipping address
                        $purchase->ship_address_line1 = Input::get('ship_address_one');
                        $purchase->ship_address_line2 = Input::get('ship_address_two');
                        $purchase->ship_address_city = Input::get('ship_city');
                        $purchase->ship_address_postcode= Input::get('ship_postcode');
                        $purchase->ship_address_county = Input::get('ship_county');
                        $purchase->shipping_name = Input::get('shipping_name');
                        $purchase->shipping_country = Input::get('shipping_country');


                        //If payment has succeeded and purchase object is successfully updated then send push and return response
                        if($purchase->save()){
                            $img = ProductImage::where('product_id', $purchase->product_id)->first();
                            if($img) {
                                $img_url = $img['base_url'].'/'.$img['public_url'];
                                $img_url  = str_replace('/original/', '/medium/', $img_url );

                                $pushData = [
                                    'notification_id' => $purchase_id,
                                    'notification_type' => 'Purchase',
                                    'channel' => 'user_'.$product->seller,
                                    'recipient' => $product->seller,
                                    'message' => $product->name.' has been sold to '.$buyer->name,
                                    'snippet' => $product->name,
                                    'extra_id' => $product->id,
                                    'img' => $img_url,
                                ];
                            } else {
                                $pushData = [
                                    'notification_id' => $purchase_id,
                                    'notification_type' => 'Purchase',
                                    'channel' => 'user_'.$product->seller,
                                    'recipient' => $product->seller,
                                    'message' => $product->name.' has been sold to '.$buyer->name,
                                    'snippet' => $product->name,
                                    'extra_id' => $product->id,
                                ];
                            }
                            $push = new NotificationController;
                            $push->sendNotification($pushData);
                            $return = ['success' => 'transaction complete'];
                            return parent::api_response($return , true, ['success' => 'purchased product '. $product->id], 200);
                        }
                    }

                }else {
                    return parent::api_response([], false, ['error' => 'Product has already been sold.'], 400);
                }
            }else{
                return parent::api_response([], false, ['error' => 'Transaction has expired.'], 400);
            }
        } catch (\Exception $e){
            return parent::api_response(['error' => $e->getMessage() ], false, ['error' => 'Could not purchase product.'], 500);
        }


    }

    protected function markDispatched($id){
        $product = Purchase::where('product_id', $id)->with('product')->first();
        if(count($product)){
            $product->status = 'dispatched';
            if($product->save()){
                $img = ProductImage::where('product_id', $id)->first();
                $img_url = $img['base_url'].'/'.$img['public_url'];
                $img_url  = str_replace('/original/', '/medium/', $img_url );
                $pushData = [
                    'notification_id' => $id,
                    'notification_type' => 'Dispatched',
                    'channel' => 'user_'.$product->buyer_id,
                    'recipient' => $product->buyer_id,
                    'message' => $product->product->name.' was marked as dispatched',
                    'snippet' => $product->product->name,
                    'extra_id' => $product->product->id,
                    'img' => $img_url,
                ];
                $push = new NotificationController;
                $push->sendNotification($pushData);
                return parent::api_response($product, true, ['success' => 'product marked as dispatched'], 200);
            }else{
                return parent::api_response([], false, ['error' => 'error marking product dispatched'], 500);
            }
        }else {
            return parent::api_response([], false, ['error' => 'Can\'t find that product.'], 404);
        }
    }

    public function createPurchase() {
        $productID = Input::get('productID');
        $product = Product
            ::where('id', $productID)
            ->firstOrFail();

        $existingPurchase = Purchase::where('product_id', $productID)->where('buyer_id', Auth::user()->id)->where('status', 'pending')->first();
        if ($existingPurchase) {
            return parent::api_response($existingPurchase, true, ['success' => 'product already reserved']);
        } elseif($product->sold) {
            return parent::api_response([], false, ['error' => 'product already sold']);
        }

        $purchase = new Purchase;
        $purchase->product_id = $productID;
        $purchase->buyer_id = Auth::user()->id;
        $purchase->status = 'pending';
        $purchase->payment_sent = 0;
        $purchase->price = $product->cost;
        $product->sold = true;

        if($purchase->save() && $product->save()){
            return parent::api_response($purchase, true, ['success' => 'purchased product '.$productID], 200);
        }else{
            return parent::api_response([], false, ['error' => 'error purchasing product'], 500);
        }
    }

    function getReceipt($id){
        $purchase = Purchase::with('product' )->find($id);
        if($purchase && $purchase->buyer_id == Auth::user()->id){
            return parent::api_response($purchase, true, ['success' => 'product marked as dispatched'], 200);
        }else{
            return parent::api_response([], false, ['error' => 'You did not buy this product'], 400);
        }
    }

    public function openDispute(Request $request, $productId)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|between:1,10000'
        ]);

        if ($validator->fails()) {
            return $this->api_response([], false, ['error' => 'message required'], 500);
        }

        $purchase = Purchase
            ::where('refunded', 0)
            ->where('product_id', $productId)
            ->first();

        if (!$purchase) {
            return $this->api_response([], false, ['error' => 'purchase not found'], 400);
        }

        if ($purchase->dispute) {
            return $this->api_response([], false, ['error' => 'purchase already disputed'], 400);
        }

        $dispute = new Dispute();
        $dispute->message = $request->input('message');
        $dispute->purchase_id = $purchase->id;
        $dispute->disputer_id = Auth::user()->id;
        $dispute->save();

        $this->dispatch(new SendAdminDisputeEmail($dispute));

        return parent::api_response([], true, ['success' => 'dispute created'], 200);
    }

    public function error () {
        return 'error';
    }

    public function clearAbandonedPurchases(){
        $purchases = Purchase::where('payment_sent', 0)->where('created_at', '<', Carbon::now()->subMinutes(15))->get();
        foreach($purchases as $purchase){
            $product = Product::find($purchase->product_id);
            $product->sold = 0;
            $product->save();
            $purchase->delete();
        }
    }

    function redirect(){
        if(Input::get('error')){
            return redirect()->away('myminimarketplace://stripe_auth?connected=0&error='.Input::get('error').'&error_description='.Input::get('error_description'));
        }
        if(Input::get('code')){
            return redirect()->away('myminimarketplace://stripe_auth?scope=read_write&connected=1&code='.Input::get('code'));
        }
    }
}