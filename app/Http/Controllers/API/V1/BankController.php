<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 16/09/2016
 * Time: 10:46
 */

namespace App\Http\Controllers\API\V1;

use App\StripeTransfer;
use App\StripeTransaction;
use App\StripeUser;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Stripe\Balance;
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Token;

class BankController extends ApiController
{
    /**
     * Get the bank account associated with the authenticated user
     *
     * @return json
     */
    public function getAccount()
    {
        $user = Auth::user();
        $data['has_stripe'] = $user->has_stripe;

        if ($user->has_stripe) {
            $data['account'] = $user->stripe->accountToArray();
        } else {
            $data['account'] = null;
        }

        return $this->api_response($data, true, 'bank account');
    }

    /**
     * Set the bank account details that are associated with the currently authenticated user
     *
     * @param Request $request The HTTP request
     * @return json
     */
    public function updateAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'address_line_one' => 'required',
            'address_city' => 'required',
            'address_county' => 'required',
            'address_country' => 'required',
            'address_postal_code' => 'required',
            'date_of_birth' => 'required|date',
            'stripe_bank_token' => 'required|sometimes',
            'account_number' => 'required_with:sort_code,account_holder_name',
            'sort_code' => 'required_with:account_number,account_holder_name',
            'account_holder_name' => 'required_with:sort_code,account_number',
        ]);

        if ($validator->fails()) {
            return parent::api_response([], false, $validator->errors()->first(), 400);
        }

        $user = Auth::user();
        $firstSetup = !$user->has_stripe;
        $account = $user->stripe;

        try {

            if (!$account) {
                $account = StripeUser::setup(
                    $user,
                    $request->ip(),
                    $request->input('first_name'),
                    $request->input('last_name')
                );
            }

            $account->updatePersonalDetails(
                $request->input('address_line_one'),
                $request->input('address_line_two'),
                $request->input('address_city'),
                $request->input('address_county'),
                $request->input('address_country'),
                $request->input('address_postal_code'),
                Carbon::parse($request->input('date_of_birth')),
                $request->input('first_name'),
                $request->input('last_name')
            );

            if ($request->has('account_number')) {
                $account->updateBankDetails(
                    $request->input('account_number'),
                    $request->input('sort_code'),
                    $request->input('account_holder_name')
                );
            }

        } catch (\Exception $e) {

            //If the user is creating their stripe account and one of the requests
            //fails delete the account and start again
            if ($firstSetup) {
                $account->delete();
                $user->save();
            }

            return parent::api_response([], false, $e->getMessage(), 500);
        }

        return $this->getAccount();
    }

    public function listTransfers()
    {
        $user = Auth::user();

        if(!$user->has_stripe) {
            return parent::api_response([], false, 'You do not have a stripe account', 400);
        }

        $transfers = StripeTransaction::where('user_id', $user->stripe->user_id)->get();

        return parent::api_response($transfers, true, 'All transfers', 200);
    }

    public function listBalance()
    {
        $user = Auth::user();
        $balance = $user->has_stripe ? $user->stripe->fetchBalance() : 0;

        return parent::api_response([
            'balance' => $balance,
        ]);
    }

    /**
     * Pay the money the user has into their bank account
     */
    public function payout()
    {
        $user = Auth::user();

        if ($user->has_stripe) {
            $user->stripe->payout();
        }

        return $this->listTransfers();
    }

    public function topup(Request $request)
    {
        try {
            Stripe::setApiKey('sk_test_SRgByjDtx11l9wiuHNY9fBOf');


            $token = Token::create(array(
                "card" => array(
                    "number" => "4000000000000077",
                    "exp_month" => 8,
                    "exp_year" => 2017,
                    "cvc" => "314"
                )
            ));

            $response = Charge::create(array(
                "amount" => $request->input('amount') * 100,
                "currency" => "gbp",
                "source" => $token, // obtained with Stripe.js
                "description" => "Topup"
            ));

            dd($response);

        } catch (Exception $e) {
            dd($e);
        }
    }

}