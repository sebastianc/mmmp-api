<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Dispute extends Model
{
    public function purchase()
    {
        return $this->belongsTo('App\Purchase');
    }

    /**
     * The title (buyer or seller) for the user that disputed the purchase.
     */
    public function getDisputerAttribute()
    {
        if (Auth::user()->id != $this->disputer_id) {
            if ($this->purchase->buyer_id == $this->disputer_id) {
                return 'Buyer';
            } else {
                return 'Seller';
            }
        }

        return '';
    }
}
