<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 12:13
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $with = ['dispute'];
    protected $appends = ['shipping_address', 'billing_address'];
    protected $hidden = [
        'refunded',
        'shipping_name',
        'ship_address_line1',
        'ship_address_line2',
        'ship_address_city',
        'ship_address_county',
        'ship_address_postcode',
        'ship_address_country',
        'address_line1',
        'address_line2',
        'address_city',
        'address_state',
        'address_zip',
        'address_country',
        'shipping_country',
    ];

    function product(){
        return $this->belongsTo('App\Product')->with('type_info', 'countries', 'genders', 'categ_info', 'subcateg_info', 'sizes', 'comments', 'commentCount', 'likes', 'images', 'seller','reviewBySeller','reviewByBuyer');
    }

    function user(){
        return $this->belongsTo('App\User', 'buyer_id');
    }

    /**
     * Morph notification type, to a notification.
     */
    public function notification()
    {
        return $this->morphMany('Notification', 'notification');
    }

    /**
     * A dispute about the purchase.
     */
    public function dispute()
    {
        return $this->hasOne('App\Dispute', 'purchase_id', 'id');
    }

    /**
     * The stripe transaction associated with a purchase
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stripeTransaction()
    {
        return $this->hasOne('App\StripeTransaction');
    }

    /**
     * Human readable status for purchases.
     *
     * @return string
     */
    public function getFormattedStatusAttribute()
    {
        switch ($this->status) {
            case 'pending': return 'Payment pending';
            case 'paid': return 'Not dispatched';
            case 'dispatched': return 'Dispatched';
        }
    }

    public function getFormattedShippingAddressAttribute()
    {
        $parts = [
            $this->ship_address_line1,
            $this->ship_address_line2,
            $this->ship_address_city,
            $this->ship_address_county,
            $this->ship_address_postcode
        ];
        $address = '';

        foreach ($parts as $part) {
            if ($part !== null && $part !== '') {
                $address .= $part . ', ';
            }
        }

        return trim($address, ', ');
    }

    public function getShippingAddressAttribute()
    {
        $address = array();
        if($this->ship_address_line1) {
            $address['name'] = $this->shipping_name;
            $address['address_line1'] = $this->ship_address_line1;
            $address['address_line2'] = $this->ship_address_line2;
            $address['address_city'] = $this->ship_address_city;
            $address['address_county'] = $this->ship_address_county;
            $address['address_zip'] = $this->ship_address_postcode;
            $address['address_country'] = $this->ship_address_country;
            return $address;
        } else {
            return null;
        }
    }

    public function getBillingAddressAttribute()
    {
        $address = array();
        if($this->address_line1) {
            $address['name'] = $this->billing_name;
            $address['address_line1'] = $this->address_line1;
            $address['address_line2'] = $this->address_line2;
            $address['address_city'] = $this->address_city;
            $address['address_county'] = $this->address_state;
            $address['address_zip'] = $this->address_zip;
            $address['address_country'] = $this->address_country;
            return $address;
        } else {
            return null;
        }
    }
}