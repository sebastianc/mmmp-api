<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 25/02/16
 * Time: 16:14
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BlockedUser extends Model
{
    function scopeCurrentUserHasBlocked($scope, $userId)
    {
        $currentUser = Auth::check() ? Auth::user()->id : null;
        return $scope->where('blocked', $userId)->where('user_id', $currentUser);
    }
}