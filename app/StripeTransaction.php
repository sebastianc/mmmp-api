<?php

namespace App;

class StripeTransaction extends BaseModel
{
    /**
     * Convert the stripe charge from a JSON string to a php array
     *
     * @param $value
     * @return array
     */
    public function getDataAttribute($value)
    {
        return json_decode($value);
    }

    function stripeUser(){
        return $this->belongsTo('App\StripeUser', 'user_id');
    }

}
