<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'phone', 'paypal_email', 'created_at', 'updated_at', 'phone_verified', 'initial_pass_reset', 'parse_id'
    ];

    /**
     * Relations to be loaded by default.
     *
     * @var array
     */
    protected $with = [
        'stripe'
    ];

    /**
     * The attributes appended to the toJSON, toArray and toDatabase methods
     *
     * @var array
     */
    protected $appends = [
        'has_stripe'
    ];

    /**
     * Determines if the user has a stripe account connected
     *
     * @return bool
     */
    function getHasStripeAttribute()
    {
        return $this->stripe !== null;
    }

    /**
     * The stripe account associated with the users account
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function stripe()
    {
        return $this->hasOne('App\StripeUser');
    }


    /**
     * Reset a users password from the token.
     *
     * @param $token
     * @param $password
     * @return bool True if the password was reset
     */
    function usePasswordReset($token, $password)
    {
        $reset = PasswordReset
            ::where('token', $token)
            ->where('email', $this->email)
            ->where('used', false)
            ->first();

        if (!$reset) {
            return false;
        }

        $reset->used = true;
        $reset->save();
        $this->password = $password;

        return true;
    }

    /**
     * Create a verification code for the users phone.
     *
     * @return VerificationCode
     */
    public function createVerificationCode()
    {
        //Only allow one verification code at a time
        VerificationCode::where('user_id', $this->id)->delete();

        $verification = new VerificationCode();
        $verification->user_id = $this->id;
        $verification->code = mt_rand(10000, 99999);
        $verification->save();

        return $verification;
    }

    /**
     * Attempt to verify the users phone using a verification code
     *
     * @param string $code The code
     * @return bool True if the phone was verified
     */
    public function useVerificationCode($code)
    {
        $code = VerificationCode::where('user_id', $this->id)->where('code', $code)->first();

        if (!$code) {
            return false;
        }

        $code->delete();
        $this->phone_verified = true;

        return true;
    }
}
