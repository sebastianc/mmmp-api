<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 11/08/2016
 * Time: 10:05
 */

namespace App;


use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class UserSearchable extends Model{

    protected $table = 'users';
    //protected $with = ['following'];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    //protected $appends = ['follow_count', 'follower_count'];

    use SoftDeletes;
    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 40,
        'username' => 20
    ];

    function toArray(){

        $array = parent::toArray();
        if(isset($array['following'])){
            if($array['following']){
                $array['following'] = true;
            }else{
                $array['following'] = false;
            }
        }
        if(isset($array['followers'])){
            $array['followers'] = count($array['followers']);
        }
        if(isset($array['follows'])){
            $array['follows'] = count($array['follows']);
        }

        if(isset($array['image'])){
            $url = $array['image'];
            if(!str_contains($url, 'graph.facebook')){
                $dimensions = [
                    'thumb' => str_replace('/original/', '/medium/', $url),
                    'large' => str_replace('/original/', '/ios/', $url),
                    'full' => $url,
                ];
            }else{
                $dimensions = [
                    'thumb' => str_replace('width=1920', 'width=400', $url),
                    'large' => str_replace('width=1920', 'width=720', $url),
                    'full' => $url,
                ];
            }


            $array['image'] = $dimensions;
        }

        return $array;
    }


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'paypal_email', 'phone', 'email','initial_pass_reset','parse_id'
    ];

    /**
     * Indicates if the current user is following the user
     */
    public function following() {
        $userId = Auth::check() ? Auth::user()->id : null;
        return $this->hasMany('App\Follow', 'follows', 'id')->where('user_id', $userId);
    }
    function reviews(){
        return $this->hasMany('App\Review');
    }

    function notifications () {
        return $this->hasMany('App\Notification');
    }

    public function followers(){
        return $this->hasMany('App\Follow', 'follows');
    }

    public function follows(){
        return $this->hasMany('App\Follow', 'user_id', 'id');
    }

    public function phone(){
        return $this->phone;
    }
    public function getEmail(){
        return $this->email;
    }
    public function thumb(){
        $url = $this->image;
        return str_replace('original', 'small', $url);
    }

    /**
     * Products the user is listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listings()
    {
        return $this->hasMany('App\Product', 'seller')->with('purchase')->where('deleted_at', null)->orderBy('created_at', 'desc');
    }

    /**
     * Products the user liked.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->belongsToMany('App\Product', 'product_likes', 'user_id', 'product_id')->where('deleted_at', null)->orderBy('product_likes.created_at', 'desc');
    }

    /**
     * Calculate the rating for a user
     */
    public function calculateRating()
    {
        $this->average_rating = Review::where(function ($query) {
                $query->where('seller_id', $this->id)->where('is_seller', 0);
            })->orWhere(function ($query) {
                $query->where('user_id', $this->id)->where('is_seller', 1);
            })->avg('rating');
    }

    /**
     * Format the rating to 1 decimal place
     *
     * @param $value
     * @return string
     */
    public function getAverageRatingAttribute($value)
    {
        return number_format($value, 1);
    }


    /**
     * Determines if the authenticated user follows the user
     */
    public function getDoesFollowAttribute()
    {
        if (!Auth::check()) {
            return false;
        }

        return Follow::where('user_id', Auth::user()->id)->where('follows', $this->id)->count() > 0;
    }

    public function getFollowCountAttribute()
    {
        return $this->follows()->count();
    }

    public function getFollowerCountAttribute()
    {
        return $this->followers()->count();
    }

    public function getLocationAttribute()
    {
        if ($this->city == null || $this->city == '') {
            return 'N/A';
        }

        return $this->city;
    }
}
