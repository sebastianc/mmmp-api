<?php

namespace App\Console\Commands;

use App\Purchase;
use App\Product;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PurchaseCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purchase:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for any reserved products 15 minutes after the purchase creation and make them available';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $purchases = Purchase::where('created_at', '<', Carbon::now()->subMinutes(15)->toDateTimeString())
            ->where('status', '<>', 'paid')
            ->get();

        if ($purchases->count()) {
            foreach ($purchases as $purchase) {
                if (!$purchase->payment_sent) {
                    if ($purchase->product->sold == true) {
                        $purchase->product->sold = false;
                        $purchase->product->marked_as_sold = false;
                        $purchase->product->save();
                    }
                    $purchase->delete();
                }
            }
        }
    }
}