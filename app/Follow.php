<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    /*
     * Morph notification type, to a notification.
     */
    public function notification ()
    {
        return $this->morphMany('Notification', 'notification');
    }

    public function users(){
        return $this->hasOne('App\UserSearchable', 'id', 'user_id');
    }

    public function follows(){
        return $this->hasOne('App\UserSearchable', 'id', 'follows');
    }

    public function following()
    {
        return $this->hasOne('App\UserSearchable', 'id', 'follows');
    }
}
