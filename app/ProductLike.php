<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 24/02/16
 * Time: 17:22
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProductLike extends Model
{
    //protected $with = ['product', 'productWeb'];
    protected $with = ['product'];

    /*
     * Morph notification type, to a notification.
     */
    public function notification ()
    {
        return $this->morphMany('Notification', 'notification');
    }

    function user(){
        return $this->belongsTo('App\User');
    }

    function product(){
        return $this->belongsTo('App\Product')->where('sold', 0)->where('marked_as_sold', 0);
    }

    /*function productWeb(){
        return $this->belongsTo('App\Product', 'product_id');
    }*/
}