<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 12/08/2016
 * Time: 09:43
 */

namespace App;

//use App\StripeAccount;
use App\StripeBankTransaction;
use App\StripeTransaction;
use App\StripeUser;
use App\User;
use DateTime;
use Illuminate\Support\Collection;
use Mockery\CountValidator\Exception;
use Stripe\Account;
use Stripe\Balance;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Recipient;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Transfer;

class ManagedStripe
{
    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SK'));
    }

    /**
     * Creates a stripe managed bank account for a user.
     *
     * @param User $user The user
     * @param DateTime $dob The users date of birth
     * @param $line1
     * @param $line2
     * @param $city
     * @param $county
     * @param $country
     * @param $postalCode
     * @return StripeUser The managed user model that was created
     */
    public function updatePersonalDetails(User $user, DateTime $dob, $line1, $line2, $city, $county, $country, $postalCode, $firstName, $lastName)
    {
        try {
            $account = Account::retrieve($user->stripe->account_id);

            $account->legal_entity->address['line1'] = $line1;
            if ($line2 !== null && trim($line2) != '') {
                $account->legal_entity->address['line2'] = $line2;
            } else {
                $account->legal_entity->address['line2'] = null;
            }
            $account->legal_entity->address['city'] = $city;
            $account->legal_entity->address['state'] = $county;
            $account->legal_entity->address['country'] = $country;
            $account->legal_entity->address['postal_code'] = $postalCode;

            $account->legal_entity->dob['day'] = $dob->format('d');
            $account->legal_entity->dob['month'] = $dob->format('m');
            $account->legal_entity->dob['year'] = $dob->format('Y');

            $account->legal_entity->first_name = $firstName;
            $account->legal_entity->last_name = $lastName;

            $account->save();

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Edit bank details for an account
     *
     * @param User $user
     * @param $accountNumber
     * @param $sortCode
     * @param $accountHolder
     * @return bool
     */
    public function updateBankAccount(User $user, $accountNumber, $sortCode, $accountHolder)
    {
        $account = $user->stripe;

        if ($account == null) {
            return false;
        }

        try {
            $account = Account::retrieve($user->stripe->account_id);

            //Remote previous accounts
            $sources = [];
            foreach ($account->external_accounts->data as $source) {
                $sources[] = $source->id;
            }

            $account->external_accounts->create([
                'external_account' => [
                    'object' => 'bank_account',
                    'account_number' => $accountNumber,
                    'routing_number' => $sortCode,
                    'account_holder_name' => $accountHolder,
                    'country' => 'GB',
                    'currency' => 'gbp'
                ],
                'default_for_currency' => true
            ]);

            foreach ($sources as $source) {
                $account->external_accounts->retrieve($source)->delete();
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Edit bank details for an account
     *
     * @param User $user
     * @param $bankToken
     * @return bool
     */
    public function updateBankAccountWithToken(User $user, $bankToken)
    {
        $account = $user->stripe;

        if ($account == null) {
            return false;
        }

        try {
            $account = Account::retrieve($user->stripe->account_id);

            //Remote previous accounts
            foreach ($account->external_accounts->data as $source) {
                $account->external_accounts->retrieve($source->id)->delete();
            }

            $account->external_accounts->create(['external_account' => $bankToken]);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Create a managed stripe account for a user
     *
     * @param User $user
     * @param $ip
     * @param $firstName
     * @param $lastName
     * @return bool
     */
    public function createAccount(User $user, $ip, $firstName, $lastName)
    {
        try {
            if (!$user->stripe) {
                
                $account = Account::create([
                    'managed' => true,
                    'country' => 'GB',
                    'email' => $user->email,
                    'legal_entity' => [
                        'type' => 'individual',
                        'first_name' => $firstName,
                        'last_name' => $lastName
                    ],
                    'tos_acceptance' => [
                        'date' => time(),
                        'ip' => $ip,
                    ],
                    "transfer_schedule" => [
                        'interval' => 'manual'
                    ],
                ]);

                $stripeManagedUser = new StripeUser();
                $stripeManagedUser->user_id = $user->id;
                $stripeManagedUser->account_id = $account->id;
                $stripeManagedUser->secret_key = $account->keys->secret;
                $stripeManagedUser->publishable_key = $account->keys->publishable;
                $stripeManagedUser->save();

                //$user->stripe->save($stripeManagedUser);

                return true;
            }
        } catch (\Exception $e) {
//            return false;
            dd($e->getMessage());
        }

        return false;
    }


    public function getAccount(User $user)
    {
        if (!$user->stripe) {
            return [
                'address_line_one' => '',
                'address_lien_two' => '',
                'address_city' => '',
                'address_county' => '',
                'address_country' => '',
                'address_postal_code' => '',
                'date_of_birth' => '',
                'sort_code' => '',
                'account_number' => '',
                'account_number_last_4' => '',
                'account_holder' => '',
                'first_name' => '',
                'last_name' => ''
            ];
        }

        $account = Account::retrieve($user->stripe->account_id);
        $legalEntity = $account->legal_entity;

        $data = [
            'address_line_one' => $legalEntity->address->line1,
            'address_line_two' => $legalEntity->address->line2,
            'address_city' => $legalEntity->address->city,
            'address_county' => $legalEntity->address->state,
            'address_country' => $legalEntity->address->country,
            'address_postal_code' => $legalEntity->address->postal_code,
            'date_of_birth' => $legalEntity->dob->day . '/' . $legalEntity->dob->month . '/' . $legalEntity->dob->year,
            'first_name' => $legalEntity->first_name,
            'last_name' => $legalEntity->last_name
        ];

        if (isset($account->external_accounts->data[0])) {
            $externalAccount = $account->external_accounts->data[0];

            $data['sort_code'] = $externalAccount->routing_number;
            $data['account_number'] = '••••' . $externalAccount->last4;
            $data['account_number_last_4'] = $externalAccount->last4;
            $data['account_holder'] = $externalAccount->account_holder_name;
        } else {
            $data['sort_code'] = '';
            $data['account_number'] = '';
            $data['account_number_last_4'] = '';
            $data['account_holder'] = '';
        }

        return $data;
    }

    public function payToUser(User $seller, $amount, $token, $purchaseId, $ip)
    {
        if (!$seller->stripe) {
            $names = explode(" ", $seller->name);
            $this->createAccount($seller, $ip, $names[0], $names[1]);
        }

        try {
            $fee = round($amount * 100 * config('myminimarketplace.fee'));

            $charge = Charge::create([
                'amount' => round($amount * 100),
                'currency' => 'gbp',
                'source' => $token,
                'destination' => $seller->stripe->account_id,
                'application_fee' => $fee,
            ]);

            $transaction = new StripeTransaction;
            $transaction->user_id = $seller->id;
            $transaction->purchase_id = $purchaseId;
            $transaction->charge_id = $charge->id;
            $transaction->data = json_encode($charge);
            $transaction->amount = $amount;
            $transaction->seller_amount = $amount - ($fee / 100);
            $transaction->save();

            return $charge;

        } catch (Exception $ex) {
            return false;
        }
    }

    public function transferToBank(User $user, $description)
    {

        if (!$user->stripe) {
            return false;
        }

        try {
            //set API key to user's to retrieve balance
            Stripe::setApiKey($user->stripe->secret_key);
            $balance = Balance::retrieve();
            $balance = $balance->available[0]->amount;

            if ($balance == 0) {
                return true;
            }

            //TODO: test further
            //Set key back to platforms
//            Stripe::setApiKey(env('STRIPE_SK'));
            $transfer = Transfer::create([
                "amount" => $balance ,
                "currency" => 'gbp',
//                "destination" => $user->stripe->account_id,
                "destination" => 'default_for_currency',
                "description" => $description
            ]);

            if ($transfer->failure_code !== null) {
                return false;
            }

            $transaction = new StripeBankTransaction();
            $transaction->user_id = $user->id;
            $transaction->stripe_id = $transfer->id;
            $transaction->amount = $balance;
            $transaction->save();

//            $user->balance = 0;
//            $user->save();

            return true;

        } catch(\Exception $e) {
            return false;
        }
    }
}