<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 26/02/16
 * Time: 10:37
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ReviewIndicator extends Model
{
    protected $table = 'reviews';
}