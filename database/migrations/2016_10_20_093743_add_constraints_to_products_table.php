<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('sold');
            $table->dropColumn('marked_as_sold');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->boolean('sold')->default(0);
            $table->boolean('marked_as_sold')->default(0);
            $table->integer('facebook_shares')->default(0);
            $table->integer('twitter_shares')->default(0);
            $table->integer('gplus_shares')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
