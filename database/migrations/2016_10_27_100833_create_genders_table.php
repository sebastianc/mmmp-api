<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::table('genders', function (Blueprint $table) {
            $sql="INSERT INTO `genders` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Female', 'female', NULL, NULL),
	(2, 'Male', 'male', NULL, NULL),
	(3, 'Unisex', 'unisex', NULL, NULL);";
            DB::statement($sql);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genders');
    }
}
