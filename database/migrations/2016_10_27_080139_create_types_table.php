<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::table('types', function (Blueprint $table) {
            $sql="INSERT INTO `types` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Preloved', 'preloved', NULL, NULL),
	(2, 'New with tags', 'new-with-tags', NULL, NULL),
	(3, 'Handmade', 'handmade', NULL, NULL),
	(4, 'Blogpost', 'blogpost', NULL, NULL);";
            DB::statement($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
