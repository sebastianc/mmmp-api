<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buyer_id')->unsigned();
            $table->foreign('buyer_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->boolean('payment_sent');
            $table->string('status');
            $table->float('price');
            $table->boolean('refunded')->default(0);
            $table->string('address_line1')->nullable();
            $table->string('address_line2')->nullable();
            $table->string('address_city', 30)->nullable();
            $table->string('address_state', 30)->nullable();
            $table->string('address_country', 30)->nullable();
            $table->string('address_zip', 20)->nullable();
            $table->string('ship_address_line1')->nullable();
            $table->string('ship_address_line2')->nullable();
            $table->string('ship_address_city', 30)->nullable();
            $table->string('ship_address_county', 30)->nullable();
            $table->string('ship_address_postcode', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
