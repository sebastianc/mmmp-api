<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSubcatNullableInProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement('ALTER TABLE `products` MODIFY `subcateg` INTEGER UNSIGNED NULL;');
            DB::statement('UPDATE `products` SET `subcateg` = NULL WHERE `subcateg` = 0;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement('UPDATE `products` SET `subcateg` = 0 WHERE `subcateg` IS NULL;');
            DB::statement('ALTER TABLE `products` MODIFY `subcateg` INTEGER UNSIGNED NOT NULL;');
        });
    }
}
