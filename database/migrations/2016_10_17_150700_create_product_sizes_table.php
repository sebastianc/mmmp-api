<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('size');
            $table->timestamps();
        });

        Schema::table('product_sizes', function (Blueprint $table) {
            $sql="INSERT INTO `product_sizes` (`id`, `size`, `created_at`, `updated_at`) VALUES
	(1, 'Newborn', NULL, NULL),
	(2, '0-3', NULL, NULL),
	(3, '3-6', NULL, NULL),
	(4, '6-12', NULL, NULL),
	(5, '12-18', NULL, NULL),
	(6, '18-24', NULL, NULL),
	(7, '1-2', NULL, NULL),
	(8, '2-3', NULL, NULL),
	(9, '3-4', NULL, NULL),
	(10, '4-5', NULL, NULL),
	(11, '5-6', NULL, NULL),
	(12, '6-7', NULL, NULL),
	(13, '7-8', NULL, NULL),
	(14, '8-9', NULL, NULL),
	(15, '9-10', NULL, NULL),
	(16, '10-11', NULL, NULL),
	(17, '11-12', NULL, NULL),
	(18, '12-13', NULL, NULL);";
            DB::statement($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sizes');
    }
}
