<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoriesAndSubcategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categs', function (Blueprint $table) {
            $sql="INSERT INTO `categs` (`id`, `name`, `slug`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'Baby & Child Clothing', 'baby-child-clothing', 'img.jpg', NULL, NULL),
	(2, 'Home & furnishings', 'home-furnishings', 'img.jpg', NULL, NULL),
	(3, 'Toys & games', 'toys-games', 'img.jpg', NULL, NULL),
	(4, 'Baby care', 'baby-care', 'img.jpg', NULL, NULL),
	(5, 'Christenings', 'christenings', 'img.jpg', NULL, NULL),
	(6, 'Children\'s parties', 'children-parties', 'img.jpg', NULL, NULL),
	(7, 'Children\'s stationery', 'children-stationery ', 'img.jpg', NULL, NULL);";
            DB::statement($sql);
        });

        Schema::table('subcategs', function (Blueprint $table) {
            $sql="INSERT INTO `subcategs` (`id`, `name`, `slug`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'Babygrows', 'babygrows', 'img.jpg', NULL, NULL),
	(2, 'Bibs', 'bibs', 'img.jpg', NULL, NULL),
	(3, 'Cardigans', 'cardigans', 'img.jpg', NULL, NULL),
	(4, 'Jumpers', 'jumpers', 'img.jpg', NULL, NULL),
	(5, 'Coats & jackets', 'coats-jackets', 'img.jpg', NULL, NULL),
	(6, 'Active wear', 'active-wear', 'img.jpg', NULL, NULL),
	(7, 'Dungarees', 'dungarees', 'img.jpg', NULL, NULL),
	(8, 'Fancy dress', 'fancy-dress', 'img.jpg', NULL, NULL),
	(9, 'Outfits & sets', 'outfits-sets', 'img.jpg', NULL, NULL),
	(10, 'Parent & me', 'parent-me', 'img.jpg', NULL, NULL),
	(11, 'Rompers', 'rompers', 'img.jpg', NULL, NULL),
	(12, 'Shirts & blouses', 'shirts-blouses', 'img.jpg', NULL, NULL),
	(13, 'Shorts', 'shorts', 'img.jpg', NULL, NULL),
	(14, 'Skirts', 'skirts', 'img.jpg', NULL, NULL),
	(15, 'Socks & tights', 'socks-tights', 'img.jpg', NULL, NULL),
	(16, 'Swimwear', 'swimwear', 'img.jpg', NULL, NULL),
	(17, 'Underwear', 'underwear', 'img.jpg', NULL, NULL),
	(18, 'Wedding', 'wedding', 'img.jpg', NULL, NULL),
	(19, 'T-shirts & tops', 't-shirts-tops', 'img.jpg', NULL, NULL),
	(20, 'Shoes & footwear', 'shoes-footwear', 'img.jpg', NULL, NULL),
	(21, 'Dresses', 'dresses', 'img.jpg', NULL, NULL),
	(22, 'Dressing Gowns & Robes', 'dressing-gowns-robes', 'img.jpg', NULL, NULL),
	(23, 'Nightwear', 'nightwear', 'img.jpg', NULL, NULL),
	(24, 'Trousers & Leggings', 'trousers-leggings', 'img.jpg', NULL, NULL),
	(25, 'Accessories', 'accessories', 'img.jpg', NULL, NULL),
	(26, 'Room Accessories', 'room-accessories', 'img.jpg', NULL, NULL),
	(27, 'Furniture', 'furniture', 'img.jpg', NULL, NULL),
	(28, 'Pictures & Paintings', 'pictures-paintings', 'img.jpg', NULL, NULL),
	(29, 'Soft Furnishings & Accessories', 'soft-furnishings-accessories', 'img.jpg', NULL, NULL),
	(30, 'Books', 'books', 'img.jpg', NULL, NULL),
	(31, 'Educational', 'educational', 'img.jpg', NULL, NULL),
	(32, 'Toy Cars & Trains', 'toy-cars-trains', 'img.jpg', NULL, NULL),
	(33, 'Soft Toys & Dolls', 'soft-toys-dolls', 'img.jpg', NULL, NULL),
	(34, 'Tents, Dens & Teepees', 'tents-dens-teepees', 'img.jpg', NULL, NULL),
	(35, 'Traditional Toys & Games', 'traditional-toys-games', 'img.jpg', NULL, NULL),
	(36, 'Play Scenes & Sets', 'play-scenes-sets', 'img.jpg', NULL, NULL),
	(37, 'Premium Toys & Games', 'premium-toys-games', 'img.jpg', NULL, NULL),
	(38, 'Outdoor Toys & Games', 'outdoor-toys-games', 'img.jpg', NULL, NULL),
	(39, 'Baby Changing', 'baby-changing', 'img.jpg', NULL, NULL),
	(40, 'Baby Sleeping Bags', 'baby-sleeping-bags', 'img.jpg', NULL, NULL),
	(41, 'Towels & Robes', 'towels-robes', 'img.jpg', NULL, NULL),
	(42, 'Blankets Comforters & Throws', 'blankets-comforters-throws', 'img.jpg', NULL, NULL),
	(43, 'Baby Care', 'baby-care', 'img.jpg', NULL, NULL),
	(44, 'Teethers & Dummies', 'teethers-dummies', 'img.jpg', NULL, NULL),
	(45, 'Gifts For Christenings', 'gifts-for-christenings', 'img.jpg', NULL, NULL),
	(46, 'Christening Wear', 'christening-wear', 'img.jpg', NULL, NULL),
	(47, 'Christening Cards', 'christening-cards', 'img.jpg', NULL, NULL),
	(48, 'Christening Invitations', 'christening-invitations', 'img.jpg', NULL, NULL);";
            DB::statement($sql);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categs', function (Blueprint $table) {
            //
        });

        Schema::table('subcategs', function (Blueprint $table) {
            //
        });
    }
}
