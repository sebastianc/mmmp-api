<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('city');
            $table->string('county');
            $table->longText('bio');
            $table->string('image');
            $table->boolean('stripe_connected')->default(0);
            $table->float('balance')->default(0);
            $table->string('facebook_id')->unique()->nullable();
            $table->float('average_rating')->default(0);

        });

        /*\App\UserSearchable::chunk(50, function ($users) {
            foreach ($users as $user) {
                $user->calculateRating();
                $user->save();
            }
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
