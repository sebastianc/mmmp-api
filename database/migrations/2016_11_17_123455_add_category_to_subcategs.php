<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToSubcategs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subcategs', function (Blueprint $table) {
            $table->integer('categ_id')->after('image')->unsigned()->default(1);
            $table->foreign('categ_id')->references('id')->on('categs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcategs', function (Blueprint $table) {
            $table->dropColumn('categ_id');
        });
    }
}
