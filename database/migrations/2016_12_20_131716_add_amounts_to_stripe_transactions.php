<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountsToStripeTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stripe_transactions', function (Blueprint $table) {
            $table->float('amount');
            $table->float('seller_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stripe_transactions', function (Blueprint $table) {
            $table->dropColumn('amount');
            $table->dropColumn('seller_amount');
        });
    }
}
