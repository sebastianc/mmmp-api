<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API ENDPOINTS (V1)
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1'], function($request) {

    Route::get('/version', 'API\V1\AuthController@update');

    //Account management
    Route::group(['prefix' => 'auth'], function ($request) {
        Route::post('/login', 'API\V1\AuthController@login');
        Route::post('/login/fb', 'API\V1\AuthController@fbLogin');
        Route::post('/register', 'API\V1\AuthController@register');
        Route::get ('/logout', 'API\V1\AuthController@logout');
        Route::post('/reset', 'API\V1\AuthController@reset');
        Route::get ('/appdata', 'API\V1\AuthController@appdata');
        Route::post('/password/request', 'API\V1\AuthController@requestPassword');
    });

    //AUTHENTICATED ROUTES
    Route::group(['middleware' => 'jwt'], function ($request) {

        /**
         * Account routes
         */
        Route::group(['prefix' => 'account'], function () {
            Route::get('/', 'API\V1\AccountController@view');
            Route::post('/push-token', 'API\V1\AccountController@savePushToken');
            Route::post('/phone/request-code', 'API\V1\AccountController@requestPhoneCode');
            Route::post('/phone/verify-code', 'API\V1\AccountController@verifyPhoneCode');
            Route::get('/bank', 'API\V1\BankController@getAccount');
            Route::get('/bank/transfers', 'API\V1\BankController@listTransfers');
            Route::get('/bank/balance', 'API\V1\BankController@listBalance');

            Route::post('/bank', 'API\V1\BankController@updateAccount');
            Route::post('/bank/payout', 'API\V1\BankController@payout');
        });

        //Products
        Route::group(['prefix' => 'products'], function ($request) {
            Route::get('/', 'API\V1\ProductController@search');
            Route::get('/followed', 'API\V1\ProductController@feed');
            Route::post('/like/{product_id}', 'API\V1\ProductController@like');
            Route::post('/unlike/{product_id}', 'API\V1\ProductController@unlike');
            Route::get('/{product_id}', 'API\V1\ProductController@get');
            //Route::post('', 'API\V1\ProductController@viewAll');
        });
        //Listings
        Route::group(['prefix' => 'listings'], function ($request) {
            //Same as profile/listings
            Route::get('/', 'API\V1\ListingController@viewAll');

            Route::post('token', 'API\V1\StripeController@createToken');
            Route::post('reserve', 'API\V1\PurchaseController@createPurchase');
            Route::post('buy', 'API\V1\PurchaseController@buy');
            Route::get('/buy/error', 'API\V1\PurchaseController@error');
            Route::get('receipt/{purchase_id}', 'API\V1\PurchaseController@getReceipt');
            Route::post('dispute/{product_id}', 'API\V1\PurchaseController@openDispute');
            Route::post('mark-dispatched/{product_id}', 'API\V1\PurchaseController@markDispatched');
            Route::post('mark-sold/{product_id}', 'API\V1\ListingController@markSold');
            Route::post('mark-available/{product_id}', 'API\V1\ListingController@markAvailable');

            //Important
            Route::post('new', 'API\V1\ListingController@upload');
            Route::post('edit', 'API\V1\ListingController@edit');

            Route::post('image/remove', 'API\V1\ListingController@removeImages');
            Route::post('/views', 'API\V1\ListingController@addViews');
            Route::post('/report/{product_id}', 'API\V1\ProductController@reportProduct');
            Route::get('/likes/{product_id}', 'API\V1\ProductController@getLikes');
        });
        //Comments
        Route::group(['prefix' => 'comments'], function ($request) {
            Route::get('/get/{product_term}', 'API\V1\CommentController@get');
            Route::post('/{product_id}', 'API\V1\CommentController@post');
        });
        //Profile
        Route::group(['prefix' => 'profile'], function ($request) {
            Route::get('/', 'API\V1\ProfileController@viewInfo');
            Route::post('', 'API\V1\ProfileController@editProfile');
            Route::get('/listings', 'API\V1\ProfileController@viewProducts');
            Route::get('/sold', 'API\V1\ProfileController@viewSoldProducts');
            Route::get('/purchased', 'API\V1\ProfileController@viewPurchased');
            Route::post('/delete/{product_id}', 'API\V1\ProfileController@delete');
            Route::get('/followers', 'API\V1\ProfileController@followers');
            Route::get('/following', 'API\V1\ProfileController@following');
            Route::get('/report/{id}', 'API\V1\ProfileController@report');

            Route::get('/reviews-buyer', 'API\V1\ProfileController@reviewsAsBuyer');
            Route::get('/reviews-seller', 'API\V1\ProfileController@reviewsAsSeller');
            Route::get('/likes/{id}', 'API\V1\ProfileController@likes');
            Route::get('/notifications', 'API\V1\NotificationController@viewAll');
            Route::get('/notifications-clear', 'API\V1\NotificationController@clearAll');
        });
        //users
        Route::group(['prefix' => 'users'], function ($request) {
            Route::get('/', 'API\V1\UserController@getUsers');
            Route::get('/rating/{user_id}', 'API\V1\UserController@getUserRating');
            Route::get('/likes/{user_id}', 'API\V1\UserController@getLikes');
            Route::get('/listings/{user_id}', 'API\V1\UserController@getProducts');
            Route::get('/followers/{user_id}', 'API\V1\UserController@getFollowers');
            Route::get('/following/{user_id}', 'API\V1\UserController@getFollowing');
            Route::get('/reviews-buyer/{user_id}', 'API\V1\UserController@reviewsAsBuyer');
            Route::get('/reviews-seller/{user_id}', 'API\V1\UserController@reviewsAsSeller');
            Route::get('/{id}/', 'API\V1\UserController@getById');
        });

        //Following users
        Route::group(['prefix' => 'follow'], function ($request) {
            Route::post('/{user_id}', 'API\V1\UserController@follow');
            Route::post('remove/{user_id}', 'API\V1\UserController@unfollow');
        });

        // FAQ
        Route::group(['prefix' => 'faq'], function () {
            Route::get('/', 'API\V1\FaqController@viewInfo');
            Route::post('/submit', 'API\V1\FaqController@createFaq');
        });

    });

});

//Redirect for app deep links
Route::get('stripe/connected', 'API\V1\PurchaseController@redirect');
Route::get('topup', 'API\V1\BankController@topup');
